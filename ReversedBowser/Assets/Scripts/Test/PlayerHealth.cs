﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MessagingSystem
{
    public class PlayerHealth : MonoBehaviour
    {
        [SerializeField]
        private bool pdirection;
        public GameObject gameoverscreen;
        [SerializeField]
        private string Scenename;

        [SerializeField]
        private float kdistance = 3f;

        public int health = 3;
        public bool PStuned = false;

         void Start()
        {
            gameoverscreen.SetActive(false);
        }
        // Update is called once per frame
        void Update()
        {
            // Player should get knockbacked when hit, we need to know where he is facing
            GameObject player = GameObject.Find("Player");
            CharacterController2D spawn = player.GetComponent<CharacterController2D>();
            pdirection = spawn.m_FacingRight;

            //

            if (health <= 0)
            {
                Message.Raise(new PlayerDead(this.gameObject));
            }
        }

        IEnumerator GameOver()
        {
            
            gameoverscreen.SetActive(true);
            yield return new WaitForSeconds(10f);
            gameoverscreen.SetActive(false);
            SceneManager.LoadScene(Scenename);

        }

        IEnumerator Knockback()
        {
            PStuned = true;
            this.gameObject.GetComponent<Collider2D>().enabled = false;
            if (pdirection == true)
            {
                this.gameObject.GetComponent<CharacterController2D>().enabled = false;
                this.gameObject.transform.position = new Vector3(transform.position.x + kdistance, transform.position.y, transform.position.z);
                yield return new WaitForSeconds(0.1f);
                this.gameObject.GetComponent<CharacterController2D>().enabled = true;
            }
            if (pdirection == false)
            {
                this.gameObject.GetComponent<CharacterController2D>().enabled = false;
                this.gameObject.transform.position = new Vector3(transform.position.x - kdistance, transform.position.y, transform.position.z);
                yield return new WaitForSeconds(0.1f);
                this.gameObject.GetComponent<CharacterController2D>().enabled = true;
            }
            yield return new WaitForSeconds(2);
            Debug.Log("Player was Knockbacked");
            this.gameObject.GetComponent<Collider2D>().enabled = true;
            PStuned = false;
        }
    }
}


