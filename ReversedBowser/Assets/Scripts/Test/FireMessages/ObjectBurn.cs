﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MessagingSystem 
{
    public class ObjectBurn : MonoBehaviour // Is nessecary to use Destroy
    {
        public GameObject Target;


        public ObjectBurn( GameObject target)
        {
            Destroy(target);
            Debug.Log("An Object was burned");
        }
    }

}
