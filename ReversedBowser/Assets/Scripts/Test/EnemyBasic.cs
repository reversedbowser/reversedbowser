﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MessagingSystem
{
    public class EnemyBasic : MonoBehaviour
    {

        [SerializeField]
        private GameObject Waypoint1;
        [SerializeField]
        private GameObject Waypoint2;
        [SerializeField]
        private bool WaypointActive = true; // serialized to check, if it works, shoud determine to what point the enemy travels, could be replaced with an int for more Waypoints
        [SerializeField]
        private float EnemySpeed = 0.01f; // eventuell scriptable objects als datencontainer?
        public int EnemyHealth = 1;

        [SerializeField] //Wip, used to see Enemystatus
        private bool EnemyStuned = false;

        [SerializeField]
        private bool playerdirection;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (EnemyStuned == false)
            {
                if (WaypointActive == true)
                {
                    transform.position = Vector2.MoveTowards(transform.position, Waypoint1.transform.position, EnemySpeed);
                    if ((Vector2.Distance(transform.position, Waypoint1.transform.position) <= 0.1)) //Should change to other Point when near or equal, 
                    {
                        WaypointActive = false;
                    }
                }
                if (WaypointActive == false)
                {
                    transform.position = Vector2.MoveTowards(transform.position, Waypoint2.transform.position, EnemySpeed);
                    if ((Vector2.Distance(transform.position, Waypoint2.transform.position) <= 0.1))
                    {
                        WaypointActive = true;
                    }
                }
            }

            //copied from other script, will maybe change this to the coroutine
            GameObject player = GameObject.Find("Player");
            CharacterController2D spawn = player.GetComponent<CharacterController2D>();

            playerdirection = spawn.m_FacingRight;

            if (EnemyHealth <= 0)
            {
                Destroy(this.gameObject);
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                GameObject player = GameObject.Find("Player");
                PlayerHealth playerHealth = player.GetComponent<PlayerHealth>();

                if (playerHealth.PStuned == false)
                {
                    StartCoroutine("EnemyHit"); // Change to Message in the future
                }
                
            }
        }

        IEnumerator EnemyHit()
        {
            EnemyStuned = true;
            EnemyHealth -= 1;


            if (playerdirection == true)
            {
                transform.Translate(1, 0, 0);
            }
            else
            {
                transform.Translate(-1, 0, 0);
            }
            yield return new WaitForSeconds(0.5f);
            Debug.Log("Enemy was hit");
        }

    }

}
