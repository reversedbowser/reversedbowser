﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindTag : MonoBehaviour
{
    public GameObject[] Burns;

    public GameObject[] Enemys;
    // Start is called before the first frame update
    void Start()
    {
        if (Enemys == null)
        {
            Enemys = GameObject.FindGameObjectsWithTag("Enemy");
        }
        foreach (GameObject Enemy in Enemys)
        {
            Debug.Log("Enemy exists");
        }

        if (Burns == null)
        {
            Burns = GameObject.FindGameObjectsWithTag("Burn");
        }
        foreach(GameObject Burn in Burns)
        {
            Debug.Log("Burns exists");
        }
    }

   
}
