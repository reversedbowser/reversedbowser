﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MessagingSystem
{
    public class EnemyHitbox : MonoBehaviour
    {


        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {

                PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
                Message.Raise(new PlayerDamage(playerHealth));
            }
        }
    }
}

