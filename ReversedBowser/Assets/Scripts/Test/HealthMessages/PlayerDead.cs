﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MessagingSystem
{
    public class PlayerDead
    {
        public GameObject player;

        public PlayerDead(GameObject Player)
        {
            Player.GetComponent<PlayerHealth>().StartCoroutine("GameOver");
            Debug.Log("Player is dead, restarting Level");
        }


    }
}

