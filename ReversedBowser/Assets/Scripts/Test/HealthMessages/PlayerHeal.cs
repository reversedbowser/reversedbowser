﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MessagingSystem
{
    public class PlayerHeal
    {
       public PlayerHeal(PlayerHealth playerHealth, int healamount)
        {
            playerHealth.health += healamount;
            Debug.Log("Player was healed by " + healamount);
        }
    }

}
