﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MessagingSystem
{
    public class PlayerDamage
    {
        public PlayerHealth playerHealth;

        public PlayerDamage(PlayerHealth phealth)
        {
            phealth.health = phealth.health - 1;
            phealth.StartCoroutine("Knockback");
            Debug.Log("Player was damaged");
        }
    }

}
