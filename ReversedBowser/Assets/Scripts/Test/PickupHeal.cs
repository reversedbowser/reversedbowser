﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MessagingSystem
{
    public class PickupHeal : MonoBehaviour
    {
        [SerializeField]
        private int healamount = 1;
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                Message.Raise(new PlayerHeal(other.gameObject.GetComponent<PlayerHealth>(), healamount));
                Destroy(this.gameObject);
            }
            
        }
    }

}
