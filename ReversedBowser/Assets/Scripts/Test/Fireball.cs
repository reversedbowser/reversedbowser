﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MessagingSystem
{
    public class Fireball : MonoBehaviour
    {
        [SerializeField]
        private bool pDirection = true; // visible so i can check its corectly given

        [SerializeField]
        private float speed = 1f;

        
        // Start is called before the first frame update
        void Awake()
        {
            
            GameObject player = GameObject.Find("Player");
            Fireballspawn spawn = player.GetComponent<Fireballspawn>();
            pDirection = spawn.PDirection;
        }

        // Update is called once per frame
        void Update()
        {

            if (pDirection == true)
            {
                transform.Translate(speed, 0, 0);
            }
            if (pDirection == false)
            {
                transform.Translate(speed * -1f, 0, 0);
            }
            //transform.position = transform.position + new Vector2()
            Destroy(this.gameObject, 1f);
        }




        public void OnTriggerEnter2D(Collider2D other)
        {
            
            //GameObject other = collision.gameObject;
            if (other.gameObject.tag =="Burn")
            {
                Debug.Log("FBurn hit");
                //Destroy(other.gameObject);  // Wip, soll 
                Message.Raise(new ObjectBurn(other.gameObject));
            }
            else if (other.gameObject.CompareTag("Enemy"))
            {
                //other.GetComponent<EnemyBasic>().StartCoroutine("EnemyHit");
                Debug.Log("FEnemy hit");
                
                Message.Raise(new EnemyBurn(other.gameObject));
            }
            else if (other.gameObject.CompareTag("Player"))
            {
                //Debug.Log("Player hit");
            }
            Debug.Log(other.gameObject.tag);

            //Just Testing
            
        }


    }


}



