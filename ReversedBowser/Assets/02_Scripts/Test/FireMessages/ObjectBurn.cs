﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MessagingSystem 
{
    public class ObjectBurn : MonoBehaviour
    {
        public GameObject Target;


        public ObjectBurn( GameObject target)
        {
            Destroy(target);
        }
    }

}
