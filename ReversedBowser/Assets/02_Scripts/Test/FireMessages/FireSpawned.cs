﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MessagingSystem
{
    public class FireSpawned : MonoBehaviour
    {
        public GameObject FPrefab;

        public Vector3 Ftransform;
        public Quaternion quaternion;

        public FireSpawned(GameObject FPrefab, Vector3 Ftransform, Quaternion quaternion)
        {
            Instantiate(FPrefab, Ftransform, quaternion);
        }
    }

}
