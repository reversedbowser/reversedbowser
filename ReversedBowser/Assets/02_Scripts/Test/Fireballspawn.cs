﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MessagingSystem
{
    public class Fireballspawn : MonoBehaviour
    {
        public GameObject FirePrefab;
        public float Fuel;

        [SerializeField]
        private CharacterController2D controller2D;

        public bool PDirection = true;

        public void Start()
        {
            Fuel = 100;

        }

        public void Update()
        {
            if (Input.GetButtonDown("Fire"))
            {
                if (Fuel >= 0)
                {
                    StartCoroutine("SpitFire");
                }

            }
            //controller2D.m_FacingRight = PDirection;
            GameObject player = GameObject.Find("Player");
            CharacterController2D spawn = player.GetComponent<CharacterController2D>();

            PDirection = spawn.m_FacingRight;
        }



        IEnumerator SpitFire()
        {
            yield return new WaitForSeconds(0.1f);
            //Instantiate(FirePrefab, transform.position, transform.rotation);
            Message.Raise(new FireSpawned(FirePrefab, transform.position, transform.rotation));
            Fuel -= 0.5f;
            yield return new WaitForSeconds(0.1f);
        }
    }

}
